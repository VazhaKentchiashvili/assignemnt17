package com.vazhasapp.assignment17

import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.vazhasapp.assignment17.databinding.ActivityMainBinding
import com.vazhasapp.assignment17.databinding.CustomDialogViewBinding
import com.vazhasapp.assignment17.extensions.setupDialogParameters

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var dialogBinding: CustomDialogViewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    private fun init() {
        binding.btnShowDialog.setOnClickListener {
            setupDialog()
        }
    }

    private fun setupDialog() {
        dialogBinding = CustomDialogViewBinding.inflate(layoutInflater)

        val dialog = Dialog(this)
        dialog.setupDialogParameters(dialogBinding)
        dialog.show()

        dialogBinding.btnCancel.setOnClickListener {
            dialog.cancel()
        }
    }
}